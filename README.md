# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

**registration** is a package for multiview point clouds registration.

+parameters for point clouds registration:

              
+   "input_pcd"                the folder name of the pcd file;

+   "baseStationName"          the name of the base station;

+   "resolutionForBsc"         the downsample resolution for binary shape context calculation

+   "resolutionForReg"         the downsample resolution for ICP

+   "radiusFeatureCalculation" the radius  for keypoint detection

+   "ratioMax"                 the ratio of lamada1 and lamada2 

+   "minPtNum"                 the minmum point number for a keypoint 

+   "radiusNonMax"             the radius  for  nonmax  suppression

+   "extract_radius"           the radius  for BSC calculation

+   "voxel_side_num"           the grid number for BSC calculation

+   "clusterNum"               the cluster number  for k-means

+   "fileName"                 the output name of whole map

+   "transformationFileName"               the name of transformation

### How do I get set up? ###


source /opt/ros/indigo/setup.bash # init environment

mkdir -p catkin_ws/src

cd catkin_ws/src

catkin_init_workspace


git clone git clone git@bitbucket.org:castacks/registration.git

cd ..

catkin_make -DCMAKE_BUILD_TYPE=Release

source devel/setup.bash

roslaunch plane_seg pcReg_node


### Who do I talk to? ###

dongzhenwhu@whu.edu.cn

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.