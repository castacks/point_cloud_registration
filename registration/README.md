# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

**registration** is a package for multiview point clouds registration.

+parameters for point clouds registration:

              
+   "input_pcd"                the folder name of the pcd file;

+   "baseStationName"          the name of the base station;

+   "resolutionForBsc"         the downsample resolution for binary shape context calculation

+   "resolutionForReg"         the downsample resolution for ICP

+   "radiusFeatureCalculation" the radius  for keypoint detection

+   "ratioMax"                 the ratio of lamada1 and lamada2 

+   "minPtNum"                 the minmum point number for a keypoint 

+   "radiusNonMax"             the radius  for  nonmax  suppression

+   "extract_radius"           the radius  for BSC calculation

+   "voxel_side_num"           the grid number for BSC calculation

+   "clusterNum"               the cluster number  for k-means

+   "fileName"                 the output name of whole map

+   "transformationFileName"               the name of transformation


### How do I get set up? ###

# Create workspace catkin_ws, fetch repositories and build: #

source /opt/ros/indigo/setup.bash # init environment

mkdir -p catkin_ws/src

cd catkin_ws/src

catkin_init_workspace


git clone git clone git@bitbucket.org:castacks/registration.git

cd ..

catkin_make -DCMAKE_BUILD_TYPE=Release

source devel/setup.bash

roslaunch plane_seg pcReg_node

### Who do I talk to? ###

dongzhenwhu@whu.edu.cn