#include "searchIndex.h"
#include <fstream>

using namespace std;

void CSearch::buildBscSearchIndex(const doubleVectorSBF &bscs, const int visualWordsNum, std::vector<SearchIndex> &searchIndex)
{
	searchIndex.resize(visualWordsNum);
	size_t gridIndex;

	for (size_t i = 0; i < bscs.size(); ++i)
	{
		for (size_t j = 0; j < bscs[i].size(); ++j)
		{
			gridIndex = bscs[i][j].bscVisualWordsIndex_;
			searchIndex[gridIndex].featuresInEachBin.push_back(bscs[i][j]);
		}
	}
}

void CSearch::buildBscSearchIndex(const vectorSBF &bscs, const int visualWordsNum, std::vector<SearchIndex> &searchIndex)
{
	searchIndex.resize(visualWordsNum);
	size_t gridIndex;

	for (size_t i = 0; i < bscs.size(); ++i)
	{
		gridIndex = bscs[i].bscVisualWordsIndex_;
		searchIndex[gridIndex].featuresInEachBin.push_back(bscs[i]);
	}
}