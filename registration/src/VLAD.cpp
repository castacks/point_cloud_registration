#include "featureEncode.h"

#include "VLAD.h"
#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/flann/flann.hpp>


using namespace cv;
using namespace std;
using namespace Eigen;

bool cmpSimilarity2(const CfeatureEncode::IdSimilarity &a, const CfeatureEncode::IdSimilarity &b)
{
	if (a.similarity > b.similarity)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void CVLAD::calculateVLAD(const TribleVectorSBF &features, int clusterNum, vectorVLAD &vlads)
{
	std::vector<StereoBinaryFeature> trainingData;
	std::vector<std::vector<StereoBinaryFeature>> testingDataSet;
	getTrainingAndTestingData(features, trainingData, testingDataSet);

	size_t trainingDataNum = trainingData.size();
	size_t featureDimension = trainingData[0].size_;
	
	//计算训练数据的Mat;
	Mat trainingDataMat(trainingDataNum, featureDimension, CV_32FC1);
	featuresToMat(trainingData, trainingDataMat);

	//用训练数据Mat通过Kmeans得到视觉单词(clusterCenters);
	cv::Mat clusterCenters(clusterNum, featureDimension, CV_32FC1);
	kmeansCluster(trainingDataMat, clusterNum, clusterCenters);
	outputClusterCenters("clusterCenters.txt", clusterCenters);

	//利用KdTree获得每个点云中每个特征的视觉单词;
	cv::flann::KDTreeIndexParams indexParams(4);
	cv::flann::Index kdtree(clusterCenters, indexParams);
#pragma omp parallel for
	for (int j = 0; j < testingDataSet.size(); ++j)
	{
		vector<int> labelSet;
		labelSet.clear();
		//计算每个测试数据（每个点云的特征集合）的Mat;
		int dataNum = testingDataSet[j].size();
		Mat dataMat(dataNum, featureDimension, CV_32FC1);
		featuresToMat(testingDataSet[j], dataMat);
		localFeatureToVisualWord(dataMat, kdtree, labelSet);
		vlads[j].resize(featureDimension*clusterNum);
		for (size_t n = 0; n < vlads[j].size(); ++n)
		{
			vlads[j](n) = 0.0f;
		}
		calculateFeatureResiduals(dataMat, clusterCenters, labelSet, vlads[j]);
		powerNormalization(vlads[j]);
		vlads[j].normalize();
	}
}

void CVLAD::calculateVLAD_DR(const TribleVectorSBF &features, int clusterNum, int compressedDimension, vectorVLAD &vlads)
{
	std::vector<StereoBinaryFeature> trainingData;
	std::vector<std::vector<StereoBinaryFeature>> testingDataSet;
	getTrainingAndTestingData(features, trainingData, testingDataSet);

	size_t trainingDataNum = trainingData.size();
	size_t featureDimension = trainingData[0].size_;

	//计算训练数据的Mat;
	Mat trainingDataMat(trainingDataNum, featureDimension, CV_32FC1);
	featuresToMat(trainingData, trainingDataMat);
	//对训练数据进行降维;
	
	Mat trainingDataComMat(trainingDataNum, compressedDimension, CV_32FC1);
	cv::PCA pca = cv::PCA(trainingDataMat, cv::Mat(), CV_PCA_DATA_AS_ROW, compressedDimension);
	pca.project(trainingDataMat, trainingDataComMat);

	//用训练数据Mat通过Kmeans得到视觉单词(clusterCenters);
	cv::Mat clusterCenters(clusterNum, compressedDimension, CV_32FC1);
	kmeansCluster(trainingDataComMat, clusterNum, clusterCenters);
	//outputClusterCenters("clusterCenters.txt", clusterCenters);

	//利用KdTree获得每个点云中每个特征的视觉单词;
	cv::flann::KDTreeIndexParams indexParams(4);
	cv::flann::Index kdtree(clusterCenters, indexParams);
#pragma omp parallel for
	for (int j = 0; j < testingDataSet.size(); ++j)
	{
		vector<int> labelSet;
		labelSet.clear();
		//计算每个测试数据（每个点云的特征集合）的Mat;
		int dataNum = testingDataSet[j].size();
		Mat dataMat(dataNum, featureDimension, CV_32FC1);
		featuresToMat(testingDataSet[j], dataMat);
		//对测试数据进行降维;
		Mat dataComMat(dataNum, compressedDimension, CV_32FC1);
		pca.project(dataMat, dataComMat);
		localFeatureToVisualWord(dataComMat, kdtree, labelSet);

		vlads[j].resize(compressedDimension*clusterNum);
		for (size_t n = 0; n < vlads[j].size(); ++n)
		{
			vlads[j](n) = 0.0f;
		}

		calculateFeatureResiduals(dataComMat, clusterCenters, labelSet, vlads[j]);
		powerNormalization(vlads[j]);
		vlads[j].normalize();
	}
}

void CVLAD::getBscVisualWordIndex(TribleVectorSBF &bsc, const int visualWordsNum)
{
	//随机选取训练数据;
	vector<StereoBinaryFeature> trainingData;
	getTrainingData(bsc, trainingData);
	int feaDimention = trainingData[0].size_;

	//计算训练数据的Mat;
	cv::Mat trainingDataMat(trainingData.size(), feaDimention, CV_32FC1);
	featuresToMat(trainingData, trainingDataMat);

	//用训练数据Mat通过K-means得到视觉单词(visualwords);
	cv::Mat visualWords(visualWordsNum, trainingData[0].size_, CV_32FC1);
	kmeansCluster(trainingDataMat, visualWordsNum, visualWords);
	
	cv::flann::KDTreeIndexParams indexParams(4);
	cv::flann::Index kdtree(visualWords, indexParams);

	//量化每一个点云的特征;
	const int k = 3;
	vector<int> indices(k);//找到点的索引
	vector<float> dists(k);

	//计算每一个BSC特征对应的视觉单词;
	for (size_t i = 0; i < bsc.size(); ++i)
	{
		for (size_t j = 0; j < bsc[i].size(); ++j)
		{
			for (size_t n = 0; n < bsc[i][j].size(); ++n)
			{
				cv::Mat bscMat(1, feaDimention, CV_32FC1);
				BscToFloatBsc(bsc[i][j][n], bscMat);
				kdtree.knnSearch(bscMat, indices, dists, k);
				bsc[i][j][n].bscVisualWordsIndex_ = indices[0];
				bsc[i][j][n].bscVisualWordsIndexV_.push_back(indices[0]);
				bsc[i][j][n].bscVisualWordsIndexV_.push_back(indices[1]);
				bsc[i][j][n].bscVisualWordsIndexV_.push_back(indices[2]);
			}
		}
	}
}

void CVLAD::calculateVLADSimilarity(const vectorVLAD &vlads, Eigen::MatrixXf &similarMatrix)
{
	Eigen::MatrixXf matrixXf(vlads.size(), vlads.size());

	for (size_t i = 0; i < vlads.size(); ++i)
	{
		for (size_t j = 0; j < vlads.size(); ++j)
		{
			if (i == j)
			{
				matrixXf(i, j) = 1.0;
			}
			else
			{
				if (i < j)
				{
					matrixXf(i, j) = calculateInnerProduct(vlads[i], vlads[j]);
				}

				else
				{
					matrixXf(i, j) = matrixXf(j, i);
				}
			}
		}
	}
	similarMatrix = matrixXf;
}

void CVLAD::getTopRankedPCPairs(const Eigen::MatrixXf &similarMatrix, int topNum, vectorPCp &pcPairs)
{
	if (topNum >= similarMatrix.cols() || topNum < 2)
	{
		cout << "please check the topNum!" << endl;
		return;
	}

	vector<IdSimilarity> vs;
	for (size_t i = 0; i < similarMatrix.rows(); ++i)
	{
		PointCloudPair candidatePair;
		candidatePair.first = i;

		vs.clear();
		for (size_t j = 0; j < similarMatrix.cols(); ++j)
		{
			IdSimilarity v;
			v.pcIndex = j;
			v.similarity = similarMatrix(i, j);
			vs.push_back(v);
		}
		sort(vs.begin(), vs.end(), cmpSimilarity2);
		for (size_t n = 1; n < topNum; ++n)
		{
			candidatePair.second = vs[n].pcIndex;
			if (isUniquePCPair(pcPairs, candidatePair))
			{
				pcPairs.push_back(candidatePair);
			}
		}
	}
	vector<IdSimilarity>().swap(vs);
}

void CVLAD::getTrainingData(const TribleVectorSBF &features, vectorSBF &trainingData)
{
	vectorSBF tempTrainingData;
	for (size_t i = 0; i < features.size(); ++i)
	{
		for (size_t j = 0; j < features[i].size(); ++j)
		{
			for (size_t n = 0; n < features[i][j].size(); ++n)
			{
				if (n % 10 == 0)
				{
					tempTrainingData.push_back(features[i][j][n]);//随机选取所有点云中的特征作为训练数据;
				}
			}
		}
	}

	if (tempTrainingData.size() >= 60000)
	{
		int interval = tempTrainingData.size() / 30000;
		for (size_t i = 0; i < tempTrainingData.size(); ++i)
		{
			if (i%interval == 0)
			{
				trainingData.push_back(tempTrainingData[i]);
			}
		}
	}
	else
	{
		trainingData = tempTrainingData;
	}
	vectorSBF().swap(tempTrainingData);
}

void CVLAD::BscToFloatBsc(const StereoBinaryFeature &bsc, cv::Mat &fBsc)
{
	for (int i = 0; i < bsc.size_; i++)
	{
		int bit_num = i % 8;
		int byte_num = i / 8;
		char test_num = 1 << bit_num;
		if (bsc.feature_[byte_num] & test_num)
		{
			fBsc.at<float>(0, i) = 1.0f;
		}
		else
		{
			fBsc.at<float>(0, i) = 0.0f;
		}
	}
}


bool  CVLAD::isUniquePCPair(const vectorPCp &pcPairs, const PointCloudPair &candidatePair)
{
	if (pcPairs.empty())
	{
		return true;
	}
	else
	{
		for (size_t i = 0; i < pcPairs.size(); ++i)
		{
			if (((pcPairs[i].first == candidatePair.first) && (pcPairs[i].second == candidatePair.second))
				|| ((pcPairs[i].first == candidatePair.second) && (pcPairs[i].second == candidatePair.first)))
			{
				return false;
			}
		}
	}
	return true;
}