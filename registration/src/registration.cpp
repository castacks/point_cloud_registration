#include "registration.h"

#include <vector>
#include <pcl/correspondence.h>
#include <pcl/registration/registration.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/registration/icp.h>

#include <boost/filesystem.hpp>
#include <boost/function.hpp>
#include <strstream>

#include <pcl/io/pcd_io.h>

using namespace std;
using namespace pcRegistration;
using namespace utility;
using namespace Eigen;
using namespace  boost::filesystem;

bool cmpPcSimilarity(const PcPairSimilarity &a, const PcPairSimilarity &b)
{
	if (a.featurePairNum > b.featurePairNum)
	{
		return true;
	}
	return false;
}


bool CmpRegPairNum(const vectorFp &a, const vectorFp &b)
{
	if (a.size() > b.size())
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*基于层次化合并的多视角点云数据配准;*/
void CReg::multiviewMatchingHPCM(const string &regFileName, const string &transformationName, const vector<string> &fileNames, const vectorPCXYZI &pcs, const vectorPCXYZI &pcsForReg,
	                             const TribleVectorSBF &bscs, const vectorVLAD &vlads, int baseStationIndex)
{
	vectorRegPair pcRegPairs;
	vector<PcPairSimilarity>  pcPairs;

	if (getCandidatePCRegPairs(vlads, pcRegPairs))
	{
		exhaustivePairWiseReg(bscs, pcRegPairs);	
		cout << "finish exhaustive PairWise Reg!" << endl;
		outputExhausiveRegPC(fileNames, pcs, pcRegPairs);
		cout << "outputExhausiveRegPC!" << endl;

		vectorPCGroup pcGroups;
		initializePCGroups(pcsForReg, bscs, pcGroups);
		cout << "initializePCGroups!" << endl;
		do 
		{
			mergeMostSimilarPcGroups(pcRegPairs, pcGroups, pcPairs);

		} while (pcGroups.size() > 1);
	}

	RegGraph graph;
	regPairsToGraph(pcPairs, graph);
	RegGraph mst;
	graphToMinSpanningTree(graph, mst);
	vector<Eigen::Matrix4f> transformations;
	getTransformationsToReference(pcPairs, baseStationIndex, mst, transformations);
	outputMatrix(fileNames, transformationName, transformations);

	outputMultiviewRegisteredPointCloudsPcd(regFileName, fileNames, transformations, pcs);
}

bool CReg::getCandidatePCRegPairs(const vectorVLAD &vlads, vectorRegPair &pcRegPairs)
{
	//计算点云之间的相似性;
	Eigen::MatrixXf similarMatrix;
	calculateVLADSimilarity(vlads, similarMatrix);

	//计算点云之间的相似性排名;
	Eigen::MatrixXi similarityRank;
	getRankedSimilarity(similarMatrix, similarityRank);
	outPutRankedSimilarity("similarityRank.txt", similarityRank);
	return getTopRankedPCRegPairs(similarMatrix, similarityRank, pcRegPairs);
}

bool CReg::getTopRankedPCRegPairs(const MatrixXf &similarMatrix, const MatrixXi &similarityRank, vectorRegPair &pcRegPairs)
{
	if (m_coRgopt.TopSimilarPCNum >= similarMatrix.cols() || m_coRgopt.TopSimilarPCNum < 2)
	{
		cout << "please check the topNum!" << endl;
		return false;
	}

	for (size_t i = 0; i < similarityRank.rows() - 1; ++i)
	{
		RegPair candidatePair;
		candidatePair.pcPair.first = i;
		for (size_t j = i + 1; j < similarityRank.cols(); ++j)
		{
			candidatePair.pcPair.second = j;
			candidatePair.minSimilarityRank = std::min(similarityRank(i, j), similarityRank(j, i));
			candidatePair.maxSimilarityRank = std::max(similarityRank(i, j), similarityRank(j, i));
			candidatePair.similarity = similarMatrix(i, j);
			if (candidatePair.minSimilarityRank <= m_coRgopt.TopSimilarPCNum)
			{
				pcRegPairs.push_back(candidatePair);
			}
		}
	}
	if (pcRegPairs.empty())
	{
		return false;
	}

	return true;
}

/*------所有可能的（VLAD相似性约束）两两配准-----*/
void CReg::exhaustivePairWiseReg(const TribleVectorSBF &bscs, vectorRegPair &pcRegPairs)
{
#pragma omp parallel for
	for (int i = 0; i < pcRegPairs.size(); ++i)
	{
		pairwiseCoarseReg(bscs, pcRegPairs[i]);
	}
}

void CReg::pairwiseCoarseReg(const TribleVectorSBF &bscs, RegPair &pcRegPair)
{
	vectorFp featurePairs;//符合特征一致性的点对集合;
	if (getFeaturePairsTwoWay(bscs[pcRegPair.pcPair.first][0], bscs[pcRegPair.pcPair.second], featurePairs))
	{
		cout << "feature Pairs:" << featurePairs.size() << endl;
		vectorFp gcPairs;
		if (getMaxGeometricConsistencyPairs(featurePairs, gcPairs))
		{
			cout << "geometric consistency:" << gcPairs.size() << endl;
			//利用符合几何一致性的点对计算转换矩阵;
			Matrix4f targetToSrc;
			calculateTransformation(gcPairs, targetToSrc);
			pcRegPair.featurePairNum = gcPairs.size();
			pcRegPair.targetToSrc = targetToSrc;
		}
	}
}

/*计算两站之间符合特征一致性的点对集合;*/
bool CReg::getFeaturePairsOneWay(const vectorSBF &featureSrc, const doubleVectorSBF &featureTar, vectorFp &featurePairs)
{
	std::vector<SearchIndex> searchIndexTar;
	buildBscSearchIndex(featureTar, m_coRgopt.visualWordsNum, searchIndexTar);

	vectorFp tempFeatPairs(featureSrc.size());

#pragma omp parallel for
	for (int i = 0; i < featureSrc.size(); ++i)
	{
		if (featureSrc[i].feature_ != nullptr)
		{
			//特征点对具有唯一性;
			if (getOptFeaturePairOneWay(featureSrc[i], searchIndexTar, tempFeatPairs[i]))
			{
				featurePairs.push_back(tempFeatPairs[i]);
			}
		}
	}

	vectorFp().swap(tempFeatPairs);
	if (featurePairs.empty())
	{
		return false;
	}
	return true;
}

bool CReg::getOptFeaturePairOneWay(const SBF &featSrc, const vector<SearchIndex> &searchIndexTar, FeaturePair &featPair)
{
	//遍历candidateFeatures, 找到与featSrc的hamming距离小于min_dis的所有点对;
	int hammingDis;
	int optimalGridIndex, optimalIndex;
	int minimalDis = (int)(featSrc.size_*m_coRgopt.tolerantHammingDis);
	int subminimalDis = (int)(featSrc.size_*m_coRgopt.tolerantHammingDis);
	int  gridIndex;
	//从 searchIndexTar 中找到视觉单词相同的candidateFeatures;
	for (int i = 0; i < 3; ++i)
	{
		gridIndex = featSrc.bscVisualWordsIndexV_[i];
		if (!searchIndexTar[gridIndex].featuresInEachBin.empty())
		{
			for (size_t n = 0; n < searchIndexTar[gridIndex].featuresInEachBin.size(); ++n)
			{
				if (searchIndexTar[gridIndex].featuresInEachBin[n].feature_ == nullptr)
				{
					continue;
				}
				else
				{
					hammingDis = hammingDistance(featSrc, searchIndexTar[gridIndex].featuresInEachBin[n]);
					if (hammingDis < minimalDis)
					{
						minimalDis = hammingDis;
						optimalGridIndex = gridIndex;
						optimalIndex = n;
					}
					else
					{
						if (hammingDis <= subminimalDis)
						{
							subminimalDis = hammingDis;
						}
					}
				}
			}
		}
	}

	//添加约束使得特征匹配具有单向的唯一性;
	if (minimalDis < subminimalDis)
	{
		featPair.first = featSrc;
		featPair.second = searchIndexTar[optimalGridIndex].featuresInEachBin[optimalIndex];
		return true;
	}
	else
	{
		return false;
	}
}

/*计算两站之间符合特征一致性的Feature点对集合;*/
bool CReg::getFeaturePairsTwoWay(const vectorSBF &featSrc, const doubleVectorSBF &featTar, vectorFp &featPairs)
{
	std::vector<SearchIndex> searchIndexTar;
	buildBscSearchIndex(featTar, m_coRgopt.visualWordsNum, searchIndexTar);

	std::vector<SearchIndex> searchIndexSrc;
	buildBscSearchIndex(featSrc, m_coRgopt.visualWordsNum, searchIndexSrc);

	vectorFp tempFeatPairs(featSrc.size());
	vector<bool> hasCorr(featSrc.size(), false);

#pragma omp parallel for
	for (int i = 0; i < featSrc.size(); ++i)
	{
		if (featSrc[i].feature_ != nullptr)
		{
			//特征点对具有单向唯一性;
			if (getOptFeaturePairOneWay(featSrc[i], searchIndexTar, tempFeatPairs[i]))
			{		
				//特征点对具有双向唯一性;
				if (getOptFeaturePairTwoWay(tempFeatPairs[i].second, searchIndexSrc, tempFeatPairs[i]))
				{
					hasCorr[i] = true;
				}			
			}
		}
	}


	for (size_t n = 0; n < hasCorr.size(); ++n)
	{
		if (hasCorr[n])
		{
			featPairs.push_back(tempFeatPairs[n]);
		}
	}

	vectorFp().swap(tempFeatPairs);
	if (featPairs.empty())
	{
		return false;
	}
	return true;
}

bool CReg::getOptFeaturePairTwoWay(const SBF &feat, const std::vector<SearchIndex> &searchIndex, const FeaturePair &featPair)
{
	FeaturePair tempFeatPair;
	if (getOptFeaturePairOneWay(feat, searchIndex, tempFeatPair))
	{
		if (tempFeatPair.second == featPair.first)
		{
			return true;
		}
	}

	return false;
}

bool CReg::getMaxGeometricConsistencyPairs(const vectorFp &featPairs, vectorFp &gcPairs)
{
	float probability = 0.002f;//内点的比例;
	float tolerance(0.999f);//希望得到的成功率;
	int minPtNum = 1;//适用于模型的最少数据个数;
	long long iterationNumT;
	iterationNumT = calculateIterationNum(probability, tolerance, minPtNum);


	//随机选取对应点对, 作为几何一致性聚类的种子点进行聚类,选择点数最多的类作为最终聚类结果;
	int iterationNum = 0;
	int featPairsIndex;

	vectorFp pairs;
	doubleVectorFp dVpairs;

	srand((unsigned)time(NULL));
	do
	{
		vectorFp().swap(pairs);
		featPairsIndex = rand() % featPairs.size();
		pairs.push_back(featPairs[featPairsIndex]);
		for (int i = 0; i < featPairs.size(); i++)
		{
			if (isCoordinateConsistency(featPairs[featPairsIndex], featPairs[i]))
			{
				pairs.push_back(featPairs[i]);
			}
		}
		dVpairs.push_back(pairs);
		iterationNum++;
	} while (iterationNum < iterationNumT);

	if (dVpairs.empty())
	{
		return false;
	}
	sort(dVpairs.begin(), dVpairs.end(), CmpRegPairNum);
	if (dVpairs[0].size() < 3)
	{
		return false;
	}
	gcPairs = dVpairs[0];


	vectorFp().swap(pairs);
	doubleVectorFp().swap(dVpairs);
	return true;
}


int CReg::calculateIterationNum(float probability, float tolerance, int minPtNum)
{
	long long iterationNumT = 0;//需要的迭代次数;
	iterationNumT = log10(1 - tolerance) / (log10(1 - pow(probability, minPtNum)));
	return iterationNumT;
}

bool CReg::isCoordinateConsistency(const FeaturePair &pair1, const FeaturePair &pair2)
{
	//计算src 坐标系;
	Vector3f xAxisSrc = pair1.first.localSystem_.xAxis;
	Vector3f yAxisSrc = pair1.first.localSystem_.yAxis;
	Vector3f zAxisSrc = pair1.first.localSystem_.zAxis;
	//计算 pair1中的first 和 pair2中的first 两个点的连线a1b1;
	Vector3f a1b1;
	a1b1 = pair2.first.localSystem_.origin - pair1.first.localSystem_.origin;
	//计算a1b1在src坐标系的坐标 ;
	float x1 = a1b1.dot(xAxisSrc);
	float y1 = a1b1.dot(yAxisSrc);
	float z1 = a1b1.dot(zAxisSrc);

	//计算tar 坐标系;
	Vector3f xAxisTar = pair1.second.localSystem_.xAxis;
	Vector3f yAxisTar = pair1.second.localSystem_.yAxis;
	Vector3f zAxisTar = pair1.second.localSystem_.zAxis;
	//计算 pair1中的second 和 pair2中的second 两个点的连线a2b2;
	Vector3f a2b2;
	a2b2 = pair2.second.localSystem_.origin - pair1.second.localSystem_.origin;
	//计算a2b2在tar坐标系的坐标 ;
	float x2 = a2b2.dot(xAxisTar);
	float y2 = a2b2.dot(yAxisTar);
	float z2 = a2b2.dot(zAxisTar);

	if (fabs(x1 - x2) > m_coRgopt.tolerantEuclideanDis / sqrt(2.0f)
		|| fabs(y1 - y2) > m_coRgopt.tolerantEuclideanDis / sqrt(2.0f)
		|| fabs(z1 - z2) > m_coRgopt.tolerantEuclideanDis / sqrt(2.0f))
	{
		return false;
	}

	return true;
}

void  CReg::calculateCoordinateInCS(const Vector3f &pt, const SBF::CoordinateSystem &cs, Vector3f &ptInCS)
{
	//计算 pt 和两个点的连线a1b1;
	Vector3f a1b1;
	a1b1 = pt - cs.origin;
	//计算a1b1在src坐标系的坐标 ;
	ptInCS.x() = a1b1.dot(cs.xAxis);
	ptInCS.y() = a1b1.dot(cs.yAxis);
	ptInCS.z() = a1b1.dot(cs.zAxis);

}

void CReg::calculateTransformation(const vectorFp &pairs, Eigen::Matrix4f &targetToSrc)
{
	/*将src和target的特征的坐标轴的原点构建PointCloud;*/
	pcXYZPtr cloud_src(new pcXYZ());
	pcXYZPtr cloud_target(new pcXYZ());
	pcl::PointXYZ pt;

	for (int i = 0; i < pairs.size(); i++)
	{
		pt.x = pairs[i].first.localSystem_.origin.x();
		pt.y = pairs[i].first.localSystem_.origin.y();
		pt.z = pairs[i].first.localSystem_.origin.z();
		cloud_src->points.push_back(pt);
	}

	for (int i = 0; i < pairs.size(); i++)
	{
		pt.x = pairs[i].second.localSystem_.origin.x();
		pt.y = pairs[i].second.localSystem_.origin.y();
		pt.z = pairs[i].second.localSystem_.origin.z();
		cloud_target->points.push_back(pt);
	}

	/*根据reg_pairs创建对应关系correspondences;*/
	pcl::Correspondences  correspondences;
	pcl::Correspondence   correspondence;
	for (int i = 0; i < pairs.size(); i++)
	{
		correspondence.index_match = i;
		correspondence.index_query = i;
		correspondences.push_back(correspondence);
	}

	//根据对应关系correspondences计算旋转平移矩阵;
	pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> trans_est;
	Eigen::Matrix4f trans;
	trans_est.estimateRigidTransformation(*cloud_src, *cloud_target, correspondences, trans);
	targetToSrc = trans.inverse();

	pcXYZ().swap(*cloud_src);
	pcXYZ().swap(*cloud_target);
}

void CReg::initializePCGroups(const vectorPCXYZI &pcs, const TribleVectorSBF &bscs, vectorPCGroup &pcGroups)
{
	pcGroups.resize(pcs.size());
	for (size_t i = 0; i < pcs.size(); ++i)
	{
		pcGroups[i].regBSCs.clear();
		pcGroups[i].regPCs.clear();
		pcGroups[i].pcIndexes.clear();

		pcGroups[i].pcGroupIndex = i;
		pcGroups[i].pcIndexes.push_back(i);
		pcGroups[i].regPCs.push_back(pcs[i]);
		pcGroups[i].regBSCs.push_back(bscs[i]);
	}
}

void  CReg::mergeMostSimilarPcGroups(const vectorRegPair &pcRegPairs, vectorPCGroup &pcGroups, vector<PcPairSimilarity>  &pcPairs)
{
	pair<int, int> pcGroupPair;
	if (getPcGroupPair(pcRegPairs, pcGroups, pcGroupPair))
	{
		vector<PcPairSimilarity> similarPcPairs;
		getTop3SimilarPcPairs(pcRegPairs, pcGroups[pcGroupPair.first], pcGroups[pcGroupPair.second], similarPcPairs);
		outputRegprocess(pcGroupPair, pcGroups, similarPcPairs);

		pcXYZI mergedSrcPC, mergedTarPC;
		vectorSBF mergedSrcFeat;
		doubleVectorSBF mergedTarFeat;

		Matrix4f coarseTrans;
		Matrix4f fineTrans;
	
		int featureNum = pairwiseCoarseReg(pcGroups[pcGroupPair.first], pcGroups[pcGroupPair.second], similarPcPairs, coarseTrans);

		cout << "pairwiseCoarseReg!" << endl;

		mergeSrcPointClouds(pcGroups[pcGroupPair.first], similarPcPairs, mergedSrcPC);
		mergeTarPointClouds(pcGroups[pcGroupPair.second], similarPcPairs, mergedTarPC);
		cout << "mergeTarPointClouds!" << endl;

		pairwiseFineReg(mergedSrcPC.makeShared(), mergedTarPC.makeShared(), coarseTrans, fineTrans);
		cout << "pairwiseFineReg!" << endl;

		PCG mergedPcGroup;
		getMergedPcGroup(pcGroups[pcGroupPair.first], pcGroups[pcGroupPair.second], fineTrans,mergedPcGroup);

		cout << "getMergedPcGroup!" << endl;

		getMergedPcPairs(pcGroups[pcGroupPair.first].pcIndexes[0], pcGroups[pcGroupPair.second].pcIndexes[0],
			             fineTrans,featureNum, pcPairs);
		cout << "getMergedPcPairs!" << endl;

		updataPcGroups(pcGroupPair, mergedPcGroup, pcGroups);
		cout << "updataPcGroups!" << endl;

	}
	else
	{
		cout << "未找到PcGroupPair。" << endl;
	}
}

bool CReg::getPcGroupPair(const vectorRegPair &pcRegPairs, const vectorPCGroup &pcGroups, pair<int, int> &pcGroupPair)
{
	bool hasPcPair = false;
	int maxSimilarity = 0;
	for (size_t i = 0; i < pcGroups.size() - 1; ++i)
	{
		for (size_t j = i + 1; j < pcGroups.size(); ++j)
		{
			int similarity = calculatePCGSimilarity(pcRegPairs, pcGroups[i], pcGroups[j]);
			if (similarity > maxSimilarity)
			{
				maxSimilarity = similarity;
				pcGroupPair.first = i;
				pcGroupPair.second = j;
				hasPcPair = true;
			}
		}
	}
	return hasPcPair;
}

int  CReg::calculatePCGSimilarity(const vectorRegPair &pcRegPairs, const PCG &pcGroup1, const PCG &pcGroup2)
{
	vector<PcPairSimilarity> similarities;
	for (size_t i = 0; i < pcGroup1.pcIndexes.size(); i++)
	{
		for (size_t j = 0; j < pcGroup2.pcIndexes.size(); ++j)
		{
			PcPairSimilarity similarity;
			int first = pcGroup1.pcIndexes[i];
			int second = pcGroup2.pcIndexes[j];
			getFeaturePair(pcRegPairs, first, second,similarity);

			similarity.pcPair.first = i;
			similarity.pcPair.second = j;
			similarities.push_back(similarity);
		}
	}

	sort(similarities.begin(), similarities.end(), cmpPcSimilarity);

	size_t num = std::min((size_t)m_coRgopt.maxMergeNum, similarities.size());

	int featureNum = 0;
	for (size_t i = 0; i < num; ++i)
	{
		featureNum += similarities[i].featurePairNum;
	}

	return featureNum;
}


bool CReg::getFeaturePair(const vectorRegPair &pcRegPairs, int pcIndex1, int pcIndex2, PcPairSimilarity& pcPair)
{
	for (size_t i = 0; i < pcRegPairs.size(); ++i)
	{
		if ((pcRegPairs[i].pcPair.first == pcIndex1&&pcRegPairs[i].pcPair.second == pcIndex2)
			|| (pcRegPairs[i].pcPair.first == pcIndex2&&pcRegPairs[i].pcPair.second == pcIndex1))
		{
			pcPair.pcPair = pcRegPairs[i].pcPair;
			pcPair.targetToSrc = pcRegPairs[i].targetToSrc;
			pcPair.featurePairNum = pcRegPairs[i].featurePairNum;

			return true;
		}
	}

	return false;
}

void CReg::getTop3SimilarPcPairs(const vectorRegPair &pcRegPairs, const PCG &pcGroupSrc, const PCG &pcGroupTar, vector<PcPairSimilarity> &pcPairs)
{
	vector<PcPairSimilarity> similarities;
	for (size_t i = 0; i < pcGroupSrc.pcIndexes.size(); i++)
	{
		for (size_t j = 0; j < pcGroupTar.pcIndexes.size(); ++j)
		{
			PcPairSimilarity similarity;
			int pcIndexFirst = pcGroupSrc.pcIndexes[i];
			int pcIndexSecond = pcGroupTar.pcIndexes[j];
			if (getFeaturePair(pcRegPairs, pcIndexFirst, pcIndexSecond, similarity))
			{
				similarity.pcPair.first = pcGroupSrc.pcIndexes[i];
				similarity.pcPair.second = pcGroupTar.pcIndexes[j];
				similarities.push_back(similarity);
			}
		}
	}
	sort(similarities.begin(), similarities.end(), cmpPcSimilarity);

	size_t num = std::min((size_t)m_coRgopt.maxMergeNum, similarities.size());
	for (size_t i = 0; i < num; ++i)
	{
		pcPairs.push_back(similarities[i]);
	}
}

void  CReg::mergeSrcPointClouds(const PCG &pcGroupSrc, const vector<PcPairSimilarity>  &pcPairs, pcXYZI & mergedSrcPC)
{
	vector<int> pcIndexesSrc;
	for (size_t i = 0; i < pcPairs.size(); ++i)
	{
		for (size_t j = 0; j < pcGroupSrc.pcIndexes.size(); ++j)
		{
			if (pcPairs[i].pcPair.first == pcGroupSrc.pcIndexes[j])
			{
				pcIndexesSrc.push_back(j);
				break;
			}
		}
	}

	sort(pcIndexesSrc.begin(), pcIndexesSrc.end());
	vector<int>::iterator new_end;
	new_end = unique(pcIndexesSrc.begin(), pcIndexesSrc.end());    //"删除"相邻的重复元素
	pcIndexesSrc.erase(new_end, pcIndexesSrc.end());  //删除（真正的删除）重复的元素

	mergedSrcPC = pcGroupSrc.regPCs[pcIndexesSrc[0]];
	int pcNum = 1;

	while (pcNum < pcIndexesSrc.size())
	{
		pcl::KdTreeFLANN<pcl::PointXYZI> tree;
		tree.setInputCloud(mergedSrcPC.makeShared());
		//邻域搜索所用变量
		vector<int> search_indices;
		vector<float> distances;
		for (size_t i = 0; i < pcGroupSrc.regPCs[pcIndexesSrc[pcNum]].points.size(); ++i)
		{
			vector<int>().swap(search_indices);
			vector<float>().swap(distances);
			//搜索邻域
			int temp = tree.radiusSearch(pcGroupSrc.regPCs[pcIndexesSrc[pcNum]].points[i], m_coRgopt.tolerantPtDis, search_indices, distances);
			if (0 == temp)
			{
				mergedSrcPC.points.push_back(pcGroupSrc.regPCs[pcIndexesSrc[pcNum]].points[i]);
			}
		}
		pcNum++;
	}
}

void  CReg::mergeTarPointClouds(const PCG &pcGroupTar, const vector<PcPairSimilarity>  &pcPairs, pcXYZI & mergedTarPC)
{
	vector<int> pcIndexesTar;
	for (size_t i = 0; i < pcPairs.size(); ++i)
	{
		for (size_t j = 0; j < pcGroupTar.pcIndexes.size(); ++j)
		{
			if (pcGroupTar.pcIndexes[j] == pcPairs[i].pcPair.second)
			{
				pcIndexesTar.push_back(j);
			}
		}
	}
	sort(pcIndexesTar.begin(), pcIndexesTar.end());
	vector<int>::iterator new_end;
	new_end = unique(pcIndexesTar.begin(), pcIndexesTar.end());    //"删除"相邻的重复元素
	pcIndexesTar.erase(new_end, pcIndexesTar.end());  //删除（真正的删除）重复的元素

	mergedTarPC = pcGroupTar.regPCs[pcIndexesTar[0]];
	int pcNum = 1;

	while (pcNum < pcIndexesTar.size())
	{
		pcl::KdTreeFLANN<pcl::PointXYZI> tree;
		tree.setInputCloud(mergedTarPC.makeShared());
		//邻域搜索所用变量
		vector<int> search_indices;
		vector<float> distances;
		for (size_t i = 0; i < pcGroupTar.regPCs[pcIndexesTar[pcNum]].points.size(); ++i)
		{
			vector<int>().swap(search_indices);
			vector<float>().swap(distances);
			//搜索邻域
			int temp = tree.radiusSearch(pcGroupTar.regPCs[pcIndexesTar[pcNum]].points[i], m_coRgopt.tolerantPtDis, search_indices, distances);
			if (0 == temp)
			{
				mergedTarPC.points.push_back(pcGroupTar.regPCs[pcIndexesTar[pcNum]].points[i]);
			}
		}
		pcNum++;
	}
}

int  CReg::pairwiseCoarseReg(const PCG &pcgSrc, const PCG &pcgTar, const vector<PcPairSimilarity>  &pcPairs, Matrix4f &targetToSrc)
{
	vectorFp featurePairs;//符合特征一致性的点对集合;
	vectorSBF featSrc;
	doubleVectorSBF featTar;

	for (size_t i = 0; i < pcPairs.size(); ++i)
	{
		vectorSBF().swap(featSrc);
		doubleVectorSBF().swap(featTar);
		
		for (size_t j = 0; j < pcgSrc.pcIndexes.size(); ++j)
		{
			if (pcgSrc.pcIndexes[j] == pcPairs[i].pcPair.first)
			{
				featSrc = pcgSrc.regBSCs[j][0];
				break;
			}
		}

		for (size_t j = 0; j < pcgTar.pcIndexes.size(); ++j)
		{
			if (pcgTar.pcIndexes[j] == pcPairs[i].pcPair.second)
			{
				featTar = pcgTar.regBSCs[j];
				break;
			}
		}
		cout << "getFeaturePairsTwoWay:" << featurePairs.size() << endl;
		getFeaturePairsTwoWay(featSrc, featTar, featurePairs);
		cout << "feature pairs number is:" << featurePairs.size() << endl;
	}

	vectorFp gcPairs;//符合特征一致性的点对集合;
	if (getMaxGeometricConsistencyPairs(featurePairs, gcPairs))
	{
		cout << "geometric consistent pair number is:" << gcPairs.size() << endl;
		cout << "-----------------------------------------" << endl;
		calculateTransformation(gcPairs, targetToSrc);
		return gcPairs.size();
	}

	cout << "the geometric consistent pair number is less than 3." << endl;
	return 0;
}

//利用ICP实现两两精配准;
float CReg::pairwiseFineReg(const pcXYZIPtr &pcSrc, const pcXYZIPtr &pcTar, const Matrix4f &coarseTrans, Matrix4f &fineTrans)
{
	double fitness_score = 0.0;
	//cloud_target根据targetToSource转换成了cloud_target_result,cloud_target_result为粗配准的结果;
	pcXYZIPtr coarseRegPcTar(new pcXYZI());

	pcl::transformPointCloud(*pcTar, *coarseRegPcTar, coarseTrans);
	fineTrans = coarseTrans;

	pcXYZIPtr tempSrcPc(new pcXYZI());//用于存储读入的数据;
	pcXYZIPtr tempTarPc(new pcXYZI());//用于存储读入的数据;
	pcXYZI tempPc;

	tempSrcPc = pcSrc;
	tempTarPc = coarseRegPcTar;

	/*精配准*/
	pcl::IterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI> icp;//创建配准对象
	//创建旋转平移矩阵
	Eigen::Matrix4f Ti;
	double euclideanFitnessEpsilon = m_reRgopt.euclideanFitnessEpsilon;
	double transformationEpsilon = m_reRgopt.transformationEpsilon;
	double maxCorrespondenceDistance = m_reRgopt.maxCorrespondenceDistance;

	while (maxCorrespondenceDistance > 0.0)
	{
		icp.setEuclideanFitnessEpsilon(euclideanFitnessEpsilon);//前后两次迭代最近点的都是距离变化值，小于该值认为已经收敛;
		icp.setTransformationEpsilon(transformationEpsilon);//前后两次迭代旋转矩阵的差异阈值，小于该值认为已经收敛;
		icp.setMaximumIterations(m_reRgopt.maximumIterations);//设置迭代的最大次数
		icp.setMaxCorrespondenceDistance(maxCorrespondenceDistance); //将两个对应关系之间的(src<->tgt)最大距离设置为50厘米，大于此值得点对不考虑。
		icp.setInputSource(tempSrcPc);
		icp.setInputTarget(tempTarPc);

		//估计
		pcXYZI().swap(tempPc);
		icp.align(tempPc);
		//在每个迭代之间累积源点云到目标点云转换矩阵Ti
		Ti = icp.getFinalTransformation();
		//得到从目标点云到源点云的变换
		Ti = Ti.inverse();
		fineTrans = Ti*fineTrans;

		pcXYZI().swap(*tempSrcPc);
		tempSrcPc = tempPc.makeShared();

		maxCorrespondenceDistance -= m_reRgopt.stepLength;
		euclideanFitnessEpsilon /= m_reRgopt.stepError;
		transformationEpsilon /= m_reRgopt.stepError;
	}

	fitness_score = icp.getFitnessScore();

	pcXYZI().swap(*tempSrcPc);
	pcXYZI().swap(*tempTarPc);
	pcXYZI().swap(*coarseRegPcTar);
	pcXYZI().swap(tempPc);
	return fitness_score;
}


void CReg::getMergedPcGroup(const PCG &pcGroupSrc, const PCG &pcGroupTar, const Matrix4f &fineTrans, PCG &mergedPcGroup)
{
	PCG pcGroupTarTrans;
	transformPcGroup(pcGroupTar, fineTrans, pcGroupTarTrans);
	mergePcGroups(pcGroupSrc, pcGroupTarTrans, mergedPcGroup);
}

void CReg::getMergedPcPairs(int srcPcIndex, int tarPcIndex, const Matrix4f &fineTrans, int featureNum, vector<PcPairSimilarity>  &pcPairs)
{
	PcPairSimilarity pair;
	pair.pcPair.first = srcPcIndex;
	pair.pcPair.second = tarPcIndex;

	pair.featurePairNum = featureNum;
	pair.targetToSrc = fineTrans;
	pcPairs.push_back(pair);
}

void CReg::transformPcGroup(const PCG &pcGroupTar, const Matrix4f &fineTrans, PCG &pcGroupTarTrans)
{
	pcGroupTarTrans = pcGroupTar;
	for (size_t i = 0; i < pcGroupTar.regPCs.size(); ++i)
	{
		transformPointCloud(pcGroupTar.regPCs[i], pcGroupTarTrans.regPCs[i], fineTrans);
		transformBsc(pcGroupTar.regBSCs[i], fineTrans, pcGroupTarTrans.regBSCs[i]);
	}
}

void CReg::transformBsc(const doubleVectorSBF &bsc, const Matrix4f &fineTrans, doubleVectorSBF &bscTrans)
{
	pcXYZ pointCloud, pointCloudTran;
	pointCloud.points.resize(4);

	for (size_t i = 0; i < bsc[0].size(); ++i)
	{
		pointCloud.points[0].x = bsc[0][i].localSystem_.origin.x();
		pointCloud.points[0].y = bsc[0][i].localSystem_.origin.y();
		pointCloud.points[0].z = bsc[0][i].localSystem_.origin.z();

		pointCloud.points[1].x = bsc[0][i].localSystem_.xAxis.x() + bsc[0][i].localSystem_.origin.x();
		pointCloud.points[1].y = bsc[0][i].localSystem_.xAxis.y() + bsc[0][i].localSystem_.origin.y();
		pointCloud.points[1].z = bsc[0][i].localSystem_.xAxis.z() + bsc[0][i].localSystem_.origin.z();

		pointCloud.points[2].x = bsc[0][i].localSystem_.yAxis.x() + bsc[0][i].localSystem_.origin.x();
		pointCloud.points[2].y = bsc[0][i].localSystem_.yAxis.y() + bsc[0][i].localSystem_.origin.y();
		pointCloud.points[2].z = bsc[0][i].localSystem_.yAxis.z() + bsc[0][i].localSystem_.origin.z();

		pointCloud.points[3].x = bsc[0][i].localSystem_.zAxis.x() + bsc[0][i].localSystem_.origin.x();
		pointCloud.points[3].y = bsc[0][i].localSystem_.zAxis.y() + bsc[0][i].localSystem_.origin.y();
		pointCloud.points[3].z = bsc[0][i].localSystem_.zAxis.z() + bsc[0][i].localSystem_.origin.z();

		transformPointCloud(pointCloud, pointCloudTran, fineTrans);
		bscTrans[0][i].localSystem_.origin.x() = pointCloudTran.points[0].x;
		bscTrans[0][i].localSystem_.origin.y() = pointCloudTran.points[0].y;
		bscTrans[0][i].localSystem_.origin.z() = pointCloudTran.points[0].z;

		bscTrans[0][i].localSystem_.xAxis.x() = pointCloudTran.points[1].x - pointCloudTran.points[0].x;
		bscTrans[0][i].localSystem_.xAxis.y() = pointCloudTran.points[1].y - pointCloudTran.points[0].y;
		bscTrans[0][i].localSystem_.xAxis.z() = pointCloudTran.points[1].z - pointCloudTran.points[0].z;
		bscTrans[0][i].localSystem_.xAxis.normalize();

		bscTrans[0][i].localSystem_.yAxis.x() = pointCloudTran.points[2].x - pointCloudTran.points[0].x;
		bscTrans[0][i].localSystem_.yAxis.y() = pointCloudTran.points[2].y - pointCloudTran.points[0].y;
		bscTrans[0][i].localSystem_.yAxis.z() = pointCloudTran.points[2].z - pointCloudTran.points[0].z;
		bscTrans[0][i].localSystem_.yAxis.normalize();

		bscTrans[0][i].localSystem_.zAxis.x() = pointCloudTran.points[3].x - pointCloudTran.points[0].x;
		bscTrans[0][i].localSystem_.zAxis.y() = pointCloudTran.points[3].y - pointCloudTran.points[0].y;
		bscTrans[0][i].localSystem_.zAxis.z() = pointCloudTran.points[3].z - pointCloudTran.points[0].z;
		bscTrans[0][i].localSystem_.zAxis.normalize();

		bscTrans[1][i].localSystem_.origin = bscTrans[0][i].localSystem_.origin;
		bscTrans[1][i].localSystem_.xAxis = -bscTrans[0][i].localSystem_.xAxis;
		bscTrans[1][i].localSystem_.yAxis = -bscTrans[0][i].localSystem_.yAxis;
		bscTrans[1][i].localSystem_.zAxis = bscTrans[0][i].localSystem_.zAxis;
	}

	pcXYZ().swap(pointCloud);
	pcXYZ().swap(pointCloudTran);
}

void CReg::mergePcGroups(const PCG &pcGroupSrc, const PCG &pcGroupTarTrans, PCG &mergedPcGroup)
{
	mergedPcGroup = pcGroupSrc;
	for (size_t i = 0; i < pcGroupTarTrans.regPCs.size(); ++i)
	{
		mergedPcGroup.pcIndexes.push_back(pcGroupTarTrans.pcIndexes[i]);
		mergedPcGroup.regBSCs.push_back(pcGroupTarTrans.regBSCs[i]);
		mergedPcGroup.regPCs.push_back(pcGroupTarTrans.regPCs[i]);
	}
}

void CReg::updataPcGroups(const pair<int, int> &pcGroupPair, const PCG &mergedPcGroup, vectorPCGroup &pcGroups)
{
	int pcGroupIndex1 = pcGroups[pcGroupPair.first].pcGroupIndex;
	int pcGroupIndex2 = pcGroups[pcGroupPair.second].pcGroupIndex;

	vectorPCGroup::iterator it;
	for (it = pcGroups.begin(); it != pcGroups.end();)
	{
		if ((*it).pcGroupIndex == pcGroupIndex1 || (*it).pcGroupIndex == pcGroupIndex2)
		{
			it = pcGroups.erase(it);    //删除元素，返回值指向已删除元素的下一个位置   
		}
		else
		{
			++it;    //指向下一个位置
		}
	}
	pcGroups.push_back(mergedPcGroup);
}


void  CReg::regPairsToGraph(const vector<PcPairSimilarity> &pcPairs, RegGraph &graph)
{
	for (size_t i = 0; i < pcPairs.size(); ++i)
	{
		boost::add_edge(pcPairs[i].pcPair.first, pcPairs[i].pcPair.second, -pcPairs[i].featurePairNum, graph);
		cout << pcPairs[i].pcPair.first << " " << pcPairs[i].pcPair.second << " " << pcPairs[i].featurePairNum << endl;
	}
}

void  CReg::graphToMinSpanningTree(const RegGraph &graph, RegGraph &mst)
{
	std::list <Edge> msTreeEdges;
	std::cout << "-----------------------" << endl;
	boost::kruskal_minimum_spanning_tree(graph, std::back_inserter(msTreeEdges));
	cout << msTreeEdges.size() << endl;

	for (std::list < Edge >::iterator it = msTreeEdges.begin(); it != msTreeEdges.end(); ++it)
	{
		cout << (*it).m_source << " " << (*it).m_target << " " << *(int*)(*it).m_eproperty << endl;
	}
	std::cout << "-----------------------" << endl;


	for (std::list <Edge>::iterator it = msTreeEdges.begin(); it != msTreeEdges.end(); ++it)
	{
		boost::add_edge((*it).m_source, (*it).m_target, *(int*)(*it).m_eproperty, mst);
	}

	for (std::list < Edge >::iterator ei = msTreeEdges.begin(); ei != msTreeEdges.end(); ++ei)
	{
		std::cout << *ei << " ";
	}
	std::cout << "\n";
	std::cout << "++++++++++++++++++" << endl;
}

void CReg::findPath(int startNodes, int endNodes, const RegGraph &mst, vector<pair<int, int>> &path)
{
	boost::vector_property_map<int> predecessors(boost::num_vertices(mst));

	predecessors[endNodes] = endNodes;

	boost::breadth_first_search(mst, endNodes, boost::visitor
		(boost::make_bfs_visitor(boost::record_predecessors
		(&predecessors[0], boost::on_tree_edge{}))));

	int p = startNodes;
	vector<int> pathNode;

	while (p != endNodes)
	{
		pathNode.push_back(p);
		cout << p << "  ";
		p = predecessors[p];
	}
	pathNode.push_back(p);
	cout << p << endl;

	if (pathNode.empty())
	{
		return;
	}

	for (size_t i = 0; i < pathNode.size() - 1; ++i)
	{
		pair<int, int> edge;
		edge.first = pathNode[i];
		edge.second = pathNode[i + 1];
		path.push_back(edge);
	}
	vector<int>().swap(pathNode);
}

void CReg::getTransformationsToReference(const vector<PcPairSimilarity>  &pcPairs, int baseStationIndex,
	const RegGraph &mst, vector<Eigen::Matrix4f> &transformations)
{
	int nodeNum = boost::num_vertices(mst);

	if (baseStationIndex < 0 || baseStationIndex >= nodeNum)
	{
		cout << "can find the base station,please check the base station index." << endl;
		return;
	}

	transformations.resize(nodeNum);

	for (size_t i = 0; i < nodeNum; ++i)
	{
		if (i == baseStationIndex)
		{
			transformations[i] = Eigen::Matrix4f::Identity(4, 4);
			continue;
		}
		vector<pair<int, int>> pathToReference;
		findPath(i, baseStationIndex, mst, pathToReference);
		getTransformation(pcPairs, pathToReference, transformations[i]);
	}
}

void CReg::getTransformation(const vector<PcPairSimilarity>  &pcPairs, const vector<pair<int, int>> &path, Eigen::Matrix4f &trans)
{
	trans = Eigen::Matrix4f::Identity(4, 4);

	for (size_t i = 0; i < path.size(); ++i)
	{
		for (size_t j = 0; j < pcPairs.size(); ++j)
		{
			if (path[i].first == pcPairs[j].pcPair.first && path[i].second == pcPairs[j].pcPair.second)
			{
				trans = pcPairs[j].targetToSrc.inverse()*trans;
				break;
			}

			if (path[i].first == pcPairs[j].pcPair.second && path[i].second == pcPairs[j].pcPair.first)
			{
				trans = pcPairs[j].targetToSrc*trans;
				break;
			}
		}
	}
}


void CReg::outputExhausiveRegPC(const vector<string> &fileNames, const vectorPCXYZI &pcs, const vectorRegPair &pcRegPairs)
{
	std::string folderName;
	folderName = "exhaustivePairWiseReg";

	//创建文件夹;
	if (boost::filesystem::exists(folderName))
	{
		directory_iterator end_iter;
		for (directory_iterator iter(folderName); iter != end_iter; ++iter)
		{
			if (is_regular_file(iter->status()))
			{
				string fileName;
				fileName = iter->path().string();
				boost::filesystem::remove(fileName);
			}
		}
	}

	if (!boost::filesystem::exists(folderName))
	{
		boost::filesystem::create_directory(folderName);
	}

	pcXYZI coarseRegPc;
	for (size_t i = 0; i < pcRegPairs.size(); ++i)
	{

		pcXYZI().swap(coarseRegPc);
		pcl::transformPointCloud(pcs[pcRegPairs[i].pcPair.second], coarseRegPc, pcRegPairs[i].targetToSrc);

		//获取不包含路径和后缀的文件名;
		std::string fileStem1, fileStem2;
		boost::filesystem::path dir1(fileNames[pcRegPairs[i].pcPair.first]);
		boost::filesystem::path dir2(fileNames[pcRegPairs[i].pcPair.second]);
		fileStem1 = dir1.stem().string();
		fileStem2 = dir2.stem().string();

		string numString;
		strstream sstr;
		sstr << pcRegPairs[i].featurePairNum;
		sstr >> numString;

		string  outputFileName;
		outputFileName = folderName + "\\" + numString + "_" + fileStem1 + "_" + fileStem2 + "_reg.pcd";
		outputRegisteredPointCloudsPcd(outputFileName, pcs[pcRegPairs[i].pcPair.first], coarseRegPc);
	}
	pcXYZI().swap(coarseRegPc);
}

void CReg::outputRegprocess(const pair<int, int> &pcGroupPair, const vectorPCGroup &pcGroups, const vector<PcPairSimilarity> &similarPcPairs)
{
	cout << "-------------------------------------" << endl;
	cout << "PCGroup " << pcGroupPair.first + 1 << " and " << pcGroupPair.second + 1 << " registration:" << endl;

	cout << "PCGroup " << pcGroupPair.first + 1 << " contains point cloud:";
	for (size_t i = 0; i < pcGroups[pcGroupPair.first].pcIndexes.size(); ++i)
	{
		cout << pcGroups[pcGroupPair.first].pcIndexes[i] + 1 << " ";
	}
	cout << endl;

	cout << "PCGroup " << pcGroupPair.second + 1 << " contains point cloud:";
	for (size_t i = 0; i < pcGroups[pcGroupPair.second].pcIndexes.size(); ++i)
	{
		cout << pcGroups[pcGroupPair.second].pcIndexes[i] + 1 << " ";
	}
	cout << endl;

	cout << "the point cloud used for registration is:" << endl;
	for (size_t i = 0; i < similarPcPairs.size(); ++i)
	{
		cout << similarPcPairs[i].pcPair.first + 1 << " and "
			<< similarPcPairs[i].pcPair.second + 1 << "  "
			<< similarPcPairs[i].featurePairNum << endl;
	}
	cout << "++++++++++" << endl;
}

void CReg::outputMultiviewRegisteredPointCloudsPcd(const string &fileName, const std::vector<string> &fileNames, const vector<Eigen::Matrix4f> &transformations, const vectorPCXYZI &pcs)
{
	std::string folderName;
	folderName = "multivewReg";

	//创建文件夹;
	if (boost::filesystem::exists(folderName))
	{
		directory_iterator end_iter;
		for (directory_iterator iter(folderName); iter != end_iter; ++iter)
		{
			if (is_regular_file(iter->status()))
			{
				string fileName;
				fileName = iter->path().string();
				boost::filesystem::remove(fileName);
			}
		}
	}

	if (!boost::filesystem::exists(folderName))
	{
		boost::filesystem::create_directory(folderName);
	}

	pcXYZI regPc;
	pcXYZI mergedPC;

	for (size_t i = 0; i < pcs.size(); ++i)
	{
		string  outputFileName;
		boost::filesystem::path dir(fileNames[i]);
		outputFileName = folderName + "\\" + "_" + dir.stem().string() + "_reg.pcd";
		cout << outputFileName << endl;

		pcXYZI().swap(regPc);
		pcl::transformPointCloud(pcs[i], regPc, transformations[i]);
		for (size_t j = 0; j < regPc.size(); ++j)
		{
			mergedPC.points.push_back(regPc.points[j]);
		}
		outputRegisteredPointCloudsPcd(outputFileName, regPc);
	}
	pcXYZI().swap(regPc);
	outputRegisteredPointCloudsPcd(fileName, mergedPC);
	pcXYZI().swap(mergedPC);
}

void CReg::outputRegisteredPointCloudsPcd(const string& fileName, pcXYZI &regPcTar)
{
	regPcTar.width = regPcTar.points.size();
	regPcTar.height = 1;
	pcl::io::savePCDFileBinary(fileName, regPcTar);
}


void CReg::outputRegisteredPointCloudsPcd(const string& fileName, const pcXYZI &pcSrc, const pcXYZI &regPcTar)
{
	pcXYZI mergedPC;
	mergedPC = pcSrc;

	for (size_t i = 0; i < regPcTar.points.size(); ++i)
	{
		mergedPC.points.push_back(regPcTar.points[i]);
	}

	mergedPC.width = pcSrc.points.size() + regPcTar.points.size();
	mergedPC.height = 1;
	pcl::io::savePCDFileBinary(fileName, mergedPC);

	pcXYZI().swap(mergedPC);
}

void CReg::outputMatrix(const std::vector<string> &fileNames, const string &fileName, const vector<Eigen::Matrix4f> &transformations)
{
	ofstream ofs(fileName);
	for (size_t i = 0; i < transformations.size(); ++i)
	{
		boost::filesystem::path dir(fileNames[i]);

		ofs << dir.stem().string() << ",";
		for (size_t j = 0; j < 4; ++j)
		{
			for (size_t n = 0; n < 4; ++n)
			{
				ofs << setiosflags(ios::fixed) << setprecision(3) << transformations[i](j, n) << ",";
			}
		}
		ofs << endl;
	}
}