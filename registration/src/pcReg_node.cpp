
#include "utility.h"
#include "dataIo.h"
#include "voxelFilter.h"
#include "principleComponentAnalysis.h"
#include "keypointDetection.h"
#include "StereoBinaryFeature.h"
#include "BinaryFeatureExtraction.h"
#include "featureEncode.h"
#include "searchIndex.h"
#include "VLAD.h"
#include "registration.h"
#include <ros/ros.h>

#include <string>
#include <omp.h>

using namespace std;
using namespace pcl;
using namespace keypoint;
using namespace pcRegistration;

int main(int argc, char**argv)
{
  
        ros::init(argc, argv,"pcReg_node");
        ros::NodeHandle("~");
     
	string input_pcd;
	ros::param::get("~input_pcd",input_pcd);
	cout<<input_pcd<<endl;
	
	string baseStationName;
	ros::param::get("~baseStationName",baseStationName);
       
	DataIo io;
	vector<string> fileNames;
	string extension = ".pcd";
	io.readFileNamesInFolder(input_pcd, extension, fileNames);
	vectorPCXYZI pointClouds(fileNames.size());
	
	
	int baseStationIndex;
	for (size_t i = 0; i < fileNames.size(); ++i)
	{
	  if(fileNames[i]==baseStationName)
	  {
	    baseStationIndex=i;
	  }
		/*第一步:数据读取;*/
		io.readPcdFile(fileNames[i], pointClouds[i]);

		cout << "finish read pcd file: " << pointClouds[i].points.size() << endl;
		if (pointClouds[i].points.empty())
			return 0;
	}

	/*第二步:数据预处理--可选项;*/
	float resolutionForBsc;//此变量控制精简大小,单位是米;
	ros::param::get("~resolutionForBsc",resolutionForBsc);

	VoxelFilter filterForBsc(resolutionForBsc);
	vectorPCXYZI pcForBsc(fileNames.size());

	float resolutionForReg;//此变量控制精简大小,单位是米;
	ros::param::get("~resolutionForReg",resolutionForReg);
	VoxelFilter filterForReg(resolutionForReg);
	vectorPCXYZI pcForReg(fileNames.size());

	/*第三步关键点检测;*/
	keypointOption kpOption;
	float radiusFeatureCalculation;
	ros::param::get("~radiusFeatureCalculation",radiusFeatureCalculation);
	kpOption.radiusFeatureCalculation = radiusFeatureCalculation;
		
	float ratioMax;
	ros::param::get("~ratioMax",ratioMax);
	kpOption.ratioMax = ratioMax;
		
	int minPtNum;
	ros::param::get("~minPtNum",minPtNum);
	kpOption.minPtNum = minPtNum;
		
	float radiusNonMax;
	ros::param::get("~radiusNonMax",radiusNonMax);
	kpOption.radiusNonMax = radiusNonMax;
	
	CkeypointDetection kpd(kpOption);
	vector<PointIndicesPtr> keyPointIndices(fileNames.size());

	/*第四步BSC特征计算*/
	float extract_radius;	//特征提取半径;
	ros::param::get("~extract_radius",extract_radius);
	
	int voxel_side_num;//每一维度的格子个数,建议取值为奇数,可以减少边缘效应对特征计算的影响;
	ros::param::get("~voxel_side_num",voxel_side_num);
	
	StereoBinaryFeatureExtractor bsc(extract_radius, voxel_side_num);//创建对象;
	TribleVectorSBF bscs(fileNames.size());
	
    #pragma omp parallel for
	for (int i = 0; i < fileNames.size(); ++i)
	{
		/*第二步:数据预处理--可选项;*/
		filterForBsc.filter(pointClouds[i],pcForBsc[i]);
		filterForReg.filter(pointClouds[i],pcForReg[i]);
		pcXYZI().swap(pointClouds[i]);

		/*第三步关键点检测;*/
		kpd.keypointDetectionBasedOnCurvature(pcForBsc[i].makeShared(), keyPointIndices[i]);
		cout << "FinishKeypointDetection!:" << keyPointIndices[i]->indices.size() << endl;

		/*第四步BSC特征计算*/
		bsc.extractBinaryFeatures(pcForBsc[i].makeShared(), keyPointIndices[i], bscs[i]);
		cout << "FinishBinaryFeaturesExtract!:" << keyPointIndices[i]->indices.size() << endl;
	}

	/*第五步:特征Encoding,计算特征Bsc的VLAD;*/
	CVLAD cl;
	vectorVLAD vlads(fileNames.size());
	int clusterNum = 200;
	ros::param::get("~clusterNum",clusterNum);
	int compressedDimension = 0.8*bscs[0][0][0].size_;
	cl.calculateVLAD_DR(bscs, clusterNum, compressedDimension, vlads);
	cout << "Finish VLAD calculation." << endl;

	//第六步: 配准;
	string fileName;
	ros::param::get("~fileName",fileName);
	
	string transformationFileName;
	ros::param::get("~transformationFileName",transformationFileName);
	
	CoarseRgOptions coRgopt;
	coRgopt.tolerantHammingDis = 0.15;
	coRgopt.tolerantEuclideanDis = 0.3f;
	coRgopt.visualWordsNum = 25;
	coRgopt.TopSimilarPCNum = 6;
	coRgopt.tolerantPtDis = resolutionForReg;
	coRgopt.maxMergeNum = 3;

	RefineRgOptions reRgopt;
	reRgopt.euclideanFitnessEpsilon = 0.0001;
	reRgopt.transformationEpsilon = reRgopt.euclideanFitnessEpsilon;
	reRgopt.maximumIterations = 2;
	reRgopt.maxCorrespondenceDistance = 0.5;
	reRgopt.stepError = 5;
	reRgopt.stepLength = 0.15;

	CReg re(coRgopt, reRgopt);
	re.getBscVisualWordIndex(bscs, coRgopt.visualWordsNum);
	re.multiviewMatchingHPCM(fileName,transformationFileName,fileNames, pcForBsc, pcForReg, bscs, vlads, baseStationIndex);
	
	return 0;
}