#include "keypointDetection.h"

#include <pcl/filters/extract_indices.h>

#include "utility.h"

#include <boost/filesystem.hpp>
#include <boost/function.hpp>

#include <fstream>
#include <string.h>

using namespace boost::filesystem;
using namespace std;
using namespace utility;
using namespace keypoint;
using namespace pcl;

bool cmpBasedOnCurvature(const CkeypointDetection::pcaFeature &a, const CkeypointDetection::pcaFeature &b)
{
	if (a.curvature > b.curvature)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CkeypointDetection::keypointDetectionBasedOnCurvature(const pcXYZIPtr &inputPointCloud, PointIndicesPtr &keypointIndices)
{
	
	vector<pcaFeature> features(inputPointCloud->points.size());
	CalculatePcaFeaturesOfPointCloud(inputPointCloud, m_option.radiusFeatureCalculation, features);

	int keypointNum = 0;

	pcl::PointIndicesPtr candidateIndices(new pcl::PointIndices());
	pruneUnstablePoints(features, m_option.ratioMax, candidateIndices);

	vector<pcaFeature> stableFeatures;
	for (size_t i = 0; i < candidateIndices->indices.size(); ++i)
	{
		stableFeatures.push_back(features[candidateIndices->indices[i]]);
	}

	pcl::PointIndicesPtr nonMaximaIndices(new pcl::PointIndices());
	nonMaximaSuppression(stableFeatures, nonMaximaIndices);
	keypointIndices = nonMaximaIndices;
	return true;
}

bool CkeypointDetection::pruneUnstablePoints( const vector<pcaFeature> &features, float ratioMax, pcl::PointIndicesPtr &indices)
{
	for (size_t i = 0; i < features.size(); ++i)
	{
		float ratio1, ratio2;
		ratio1 = features[i].values.lamada2 / features[i].values.lamada1;
		ratio2 = features[i].values.lamada3 / features[i].values.lamada2;

		if (ratio1 < ratioMax && ratio2 < ratioMax && features[i].ptNum > m_option.minPtNum)
		{
			indices->indices.push_back(i);
		}
	}

	return true;
}


bool CkeypointDetection::nonMaximaSuppression(vector<CkeypointDetection::pcaFeature> &features, pcl::PointIndicesPtr &indices)
{
	sort(features.begin(), features.end(), cmpBasedOnCurvature);
	pcl::PointCloud<pcl::PointXYZI> pointCloud;

	/*建立UnSegment以及UnSegment的迭代器,存储未分割的点号;*/
	set<size_t, less<size_t> > unVisitedPtId;
	set<size_t, less<size_t> >::iterator iterUnseg;
	for (size_t i = 0; i < features.size(); ++i)
	{
		unVisitedPtId.insert(i);
		pointCloud.points.push_back(features[i].pt);
	}

	pcl::KdTreeFLANN<pcl::PointXYZI> tree;
	tree.setInputCloud(pointCloud.makeShared());

	//邻域搜索所用变量
	vector<int> search_indices;
	vector<float> distances;

	size_t keypointNum = 0;
	do 
	{
		keypointNum++;
		vector<int>().swap(search_indices);
		vector<float>().swap(distances);

		size_t id;
		iterUnseg = unVisitedPtId.begin();
		id = *iterUnseg;
		indices->indices.push_back(features[id].ptId);
		unVisitedPtId.erase(id);

		tree.radiusSearch(features[id].pt, m_option.radiusNonMax, search_indices, distances);

		for (size_t i = 0; i < search_indices.size(); ++i)
		{
			unVisitedPtId.erase(search_indices[i]);
		}

	} while (!unVisitedPtId.empty());

	return true;
}