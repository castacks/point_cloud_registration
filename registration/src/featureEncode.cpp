#include "featureEncode.h"
#include "utility.h"

using namespace  cv;
using namespace Eigen;

bool cmpSimilarity(const CfeatureEncode::IdSimilarity &a, const CfeatureEncode::IdSimilarity &b)
{
	if (a.similarity > b.similarity)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void CfeatureEncode::localFeatureToVisualWord(const cv::Mat &featureMat, cv::flann::Index &kdtree, std::vector<int> &labels)
{
	//量化每一个点云的特征;
	const int k = 1;
	vector<int> indices(k);//找到点的索引
	vector<float> dists(k);

	for (size_t j = 0; j < featureMat.rows; ++j)
	{
		kdtree.knnSearch(featureMat.row(j), indices, dists, k);
		labels.push_back(indices[0]);
	}
}

void CfeatureEncode::getRankedSimilarity(const Eigen::MatrixXf &matrixXf, Eigen::MatrixXi &similarityRank)
{
	Eigen::MatrixXi rank(matrixXf.rows(), matrixXf.cols());
	vector<IdSimilarity> vs;

	for (size_t i = 0; i < matrixXf.rows(); ++i)
	{
		vs.clear();

		for (size_t j = 0; j < matrixXf.cols(); ++j)
		{
			IdSimilarity v;
			v.pcIndex = j;
			v.similarity = matrixXf(i, j);
			vs.push_back(v);
		}

		sort(vs.begin(), vs.end(), cmpSimilarity);
		
		for (size_t j = 0; j < vs.size(); ++j)
		{
			rank(i, vs[j].pcIndex) = j;
		}
	}

	similarityRank = rank;
	vector<IdSimilarity>().swap(vs);
}

void CfeatureEncode::calculateResidual(const vector<float> &localFeature, int visualWordLabel, const vector<float> &visualWord, Eigen::VectorXf &residuals)
{
	int dimension = localFeature.size();
	for (size_t i = 0; i < dimension; ++i)
	{
		residuals(visualWordLabel*dimension + i) += (localFeature[i] - visualWord[i]);
	}
}

void CfeatureEncode::calculateFeatureResiduals(const Mat &featureMat, const Mat &centers, const vector<int> &labels, VectorXf &residuals)
{
	//计算每个测试数据（每个点云的特征集合）的Mat;
	int dataNum = featureMat.rows;
	for (size_t i = 0; i < dataNum; i++)
	{
		calculateResidual(featureMat.row(i), labels[i], centers.row(labels[i]), residuals);
	}
}


void CfeatureEncode::powerNormalization(Eigen::VectorXf &vlad)
{
	for (size_t i = 0; i < vlad.size(); ++i)
	{
		if (vlad(i) < 0.0f)
		{
			vlad(i) = -1.0*sqrt(-vlad(i));
		}
		else
		{
			if (vlad(i) > 0.0f)
			{
				vlad(i) = sqrt(vlad(i));
			}
			else
			{
				vlad(i) = 0.0f;
			}
		}
	}
}


//L2norm 归一化使得每个visualword(cluster)的贡献相同;
void CfeatureEncode::EachClusterL2NormNormalization(const int dimension, Eigen::VectorXf &rvd)
{
	int visualWordsNum = rvd.size() / dimension;
	vector<double>norms(visualWordsNum);

	for (size_t i = 0; i < visualWordsNum; ++i)
	{
		double norm = 0;
		for (size_t j = 0; j < dimension; ++j)
		{
			norm += (rvd(i*dimension + j)*rvd(i*dimension + j));
		}
		norms[i] = norm;
	}

	for (size_t i = 0; i < visualWordsNum; ++i)
	{
		for (size_t j = 0; j < dimension; ++j)
		{
			rvd(i*dimension + j) /= norms[i];
		}
	}
}


float CfeatureEncode::calculateVectorNorm1(const std::vector<float> &residual)
{
	float norm1 = 0.0f;
	for (size_t i = 0; i < residual.size(); ++i)
	{
		norm1 += fabs(residual[i]);
	}

	return norm1;
}
void CfeatureEncode::calculateFeatureSimilarity(const std::vector<Eigen::VectorXf> &vlads, Eigen::MatrixXf &matrixXf)
{
	Eigen::MatrixXf similarMatrix(vlads.size(), vlads.size());

	for (size_t i = 0; i < vlads.size(); ++i)
	{
		for (size_t j = 0; j < vlads.size(); ++j)
		{
			if (i == j)
			{
				similarMatrix(i, j) = 1.0;
			}
			else
			{
				if (i < j)
				{
					similarMatrix(i, j) = calculateInnerProduct(vlads[i], vlads[j]);
				}

				else
				{
					similarMatrix(i, j) = similarMatrix(j, i);
				}
			}
		}
	}
	matrixXf = similarMatrix;
}

float CfeatureEncode::calculateInnerProduct(const Eigen::VectorXf & f1, const Eigen::VectorXf & f2)
{
	float dotProduct;
	dotProduct = f1.dot(f2);

	if (dotProduct > 1.0)
	{
		dotProduct = 1.0;
	}

	if (dotProduct < -1.0)
	{
		dotProduct = -1.0;
	}

	return dotProduct;
}


float CfeatureEncode::calculateIncludeAngle(const Eigen::VectorXf & f1, const Eigen::VectorXf & f2)
{
	float dotProduct, includeAngle;
	dotProduct = f1.dot(f2);

	if (dotProduct > 1.0)
	{
		dotProduct = 1.0;
	}

	if (dotProduct < -1.0)
	{
		dotProduct = -1.0;
	}

	includeAngle = acos(dotProduct);

	return includeAngle;
}

void CfeatureEncode::outPutRankedSimilarity(const std::string &fileName, const Eigen::MatrixXf &matrixXf)
{
	std::ofstream ofs;
	ofs.open(fileName, std::ios::out);
	vector<IdSimilarity> vs;
	if (ofs.is_open())
	{
		for (size_t i = 0; i < matrixXf.rows(); ++i)
		{
			vs.clear();
			for (size_t j = 0; j < matrixXf.cols(); ++j)
			{
				IdSimilarity v;
				v.pcIndex = j;
				v.similarity = matrixXf(i, j);
				vs.push_back(v);
			}
			sort(vs.begin(), vs.end(), cmpSimilarity);
			ofs << i + 1 << " ";
			for (size_t n = 1; n < vs.size(); ++n)
			{
				ofs << vs[n].pcIndex + 1 << " ";
				ofs << setiosflags(ios::fixed) << setprecision(3) << vs[n].similarity << " ";
			}
			ofs << endl;
		}
	}
	ofs.close();
	vector<IdSimilarity>().swap(vs);
}


void CfeatureEncode::outPutRankedSimilarity(const std::string &fileName, const Eigen::MatrixXi &similarityRank)
{
	std::ofstream ofs;
	ofs.open(fileName, std::ios::out);
	if (ofs.is_open())
	{
		for (size_t i = 0; i < similarityRank.rows(); ++i)
		{
			for (size_t j = 0; j < similarityRank.cols(); ++j)
			{
				ofs << similarityRank(i,j) << " ";
			}
			ofs << endl;
		}
	}
	ofs.close();
}

void CfeatureEncode::outPutSimilarity(const std::string &fileName, const Eigen::MatrixXf &matrixXf)
{
	std::ofstream ofs;
	ofs.open(fileName, std::ios::out);
	if (ofs.is_open())
	{
		ofs << matrixXf.rows() << endl;
		for (size_t i = 0; i < matrixXf.rows(); ++i)
		{
			for (size_t j = 0; j < matrixXf.cols(); ++j)
			{
				ofs << setiosflags(ios::fixed) << setprecision(3) << matrixXf(i, j) << " ";

			}
			ofs << endl;
		}
	}
	ofs.close();
}


void CfeatureEncode::pcaDReduction(const cv::Mat &dataMat, const int compressedDimension, cv::Mat &CompressedDataMat)
{
	cv::PCA pca = cv::PCA(dataMat, cv::Mat(), CV_PCA_DATA_AS_ROW, compressedDimension);
	pca.project(dataMat, CompressedDataMat);
}

void CfeatureEncode::getTrainingAndTestingData(const TribleVectorSBF &features,vectorSBF &trainingData, doubleVectorSBF &testingData)
{
	vectorSBF pointCloudFeatures;
	vectorSBF tempTrainingData;

	for (size_t i = 0; i < features.size(); ++i)
	{
		pointCloudFeatures.clear();
		for (size_t j = 0; j < features[i].size(); ++j)
		{
			for (size_t n = 0; n < features[i][j].size(); ++n)
			{
				if (n % 10 == 0)
				{
					tempTrainingData.push_back(features[i][j][n]);//随机选取所有点云中的特征作为训练数据;
				}

				pointCloudFeatures.push_back(features[i][j][n]);
			}
		}
		testingData.push_back(pointCloudFeatures);
	}
	std::vector<StereoBinaryFeature>().swap(pointCloudFeatures);

	if (tempTrainingData.size() >= 60000)
	{
		int interval = tempTrainingData.size() / 30000;
		for (size_t i = 0; i < tempTrainingData.size(); ++i)
		{
			if (i%interval == 0)
			{
				trainingData.push_back(tempTrainingData[i]);
			}
		}
	}
	else
	{
		trainingData = tempTrainingData;
	}

	vectorSBF().swap(pointCloudFeatures);
	vectorSBF().swap(tempTrainingData);
}


void CfeatureEncode::kmeansCluster(const cv::Mat &trainingData, int clusterNum, cv::Mat &centers)
{
	TermCriteria criteria = TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 10, 0.1);
	cv::Mat labels;
	kmeans(trainingData, clusterNum, labels, criteria, 3, KMEANS_PP_CENTERS, centers);
}


void CfeatureEncode::displayClusterResult(const pcXYZI &pointCloud, const cv::Mat &labels, const cv::Mat &centers)
{

}

void CfeatureEncode::charToBit(const char num, vector<float> &bits)
{
	int count = 0;
	unsigned int flag = 1;

	while (flag <= 128)
	{
		if (num & flag)
		{
			bits.push_back(1.0f);
		}
		else
		{
			bits.push_back(0.0f);
		}
		flag = flag << 1;
	}
}

void CfeatureEncode::BscToFloatBsc(const StereoBinaryFeature &bsc, vector<float> &fBsc)
{
	//cout << bsc.size_ << endl;
	for (int i = 0; i < bsc.size_; i++)
	{
		int bit_num = i % 8;
		int byte_num = i / 8;
		char test_num = 1 << bit_num;
		if (bsc.feature_[byte_num] & test_num)
		{
			fBsc.push_back(1.0f);
		}
		else
		{
			fBsc.push_back(0.0f);
		}
	}
}


void CfeatureEncode::featuresToMat(const vectorSBF &features, cv::Mat &featureMat)
{
	int dataNum = features.size();
	int featureDimension = features[0].size_;

	for (size_t i = 0; i < dataNum; ++i)
	{
		vector<float> feature;
		feature.clear();
		BscToFloatBsc(features[i], feature);

		for (size_t j = 0; j < featureDimension; ++j)
		{
			featureMat.at<float>(i, j) = feature[j];
		}
	}
}

void CfeatureEncode::writeFeatures(const string &fileName, const StereoBinaryFeature &feature)
{
	ofstream ofs(fileName);
	int featureDimension = feature.size_;
	vector<float> featureV;
	BscToFloatBsc(feature, featureV);

	for (size_t i = 0; i < featureDimension; ++i)
	{
		ofs << setiosflags(ios::fixed)<<setprecision(3)<<featureV[i] << endl;
	}
}

void CfeatureEncode::outputClusterCenters(const std::string & fileName, const cv::Mat &centers)
{
	ofstream ofs(fileName);

	ofs << centers.rows << " "<<centers.cols << endl;
	for (size_t i = 0; i < centers.rows; ++i)
	{
		for (size_t j = 0; j < centers.cols; ++j)
		{
			ofs << setiosflags(ios::fixed) << setprecision(3) << centers.at<float>(i, j) << " ";
		}
		ofs << endl;
	}
}

void CfeatureEncode::outputCompactFeatures(const std::string & fileName, const std::vector<Eigen::VectorXf> &features)
{
	ofstream ofs(fileName);

	for (size_t i = 0; i < features.size(); i++)
	{
		for (size_t j = 0; j < features[i].size(); ++j)
		{
			ofs << setiosflags(ios::fixed) << setprecision(3) << features[i][j] << " ";
		}
		ofs << endl;
	}
}