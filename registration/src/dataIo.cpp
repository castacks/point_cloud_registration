#include "dataIo.h"
#include "utility.h"

#include <string>
#include <fstream>
#include <vector>
#include <pcl/io/pcd_io.h>



using namespace  std;
using namespace  boost::filesystem;
using namespace  utility;

bool DataIo::readFileNamesInFolder(const string &folderName, const string & extension,vector<string> &fileNames)
{
	//利用boost遍历文件夹内的文件;
	if (!exists(folderName))
	{
		return 0;
	}
	else
	{
		//遍历该文件夹下的所有ply文件,并保存到fileNames中;
		directory_iterator end_iter;
		for (directory_iterator iter(folderName); iter != end_iter; ++iter)
		{
			if (is_regular_file(iter->status()))
			{
				string fileName;
				fileName = iter->path().string();

				path dir(fileName);

				if (!dir.extension().string().empty())
				{
					if (!fileName.substr(fileName.rfind('.')).compare(extension))
					{
						fileNames.push_back(fileName);
					}
				}
			}
		}
	}
	return 1;
}

bool DataIo::readPcdFile(const std::string &fileName, pcl::PointCloud<pcl::PointXYZI> &pointCloud)
{
	if (pcl::io::loadPCDFile<pcl::PointXYZI>(fileName, pointCloud) == -1) //* load the file
	{
		PCL_ERROR("Couldn't read file test_pcd.pcd \n");
		return false;
	}

	return true;
}