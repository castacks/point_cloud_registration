#ifndef DATAIO 
#define DATAIO

#include <string>
#include <string.h>

#include<pcl/point_cloud.h>
#include<pcl/point_types.h>

#include <vector>
#include <boost/filesystem.hpp>
#include <boost/function.hpp>

using namespace std;
class DataIo
{
public:
	//文件夹遍历;
	bool readFileNamesInFolder(const string &folderName,const string & extension,vector<string> &fileNames);
	// pcd 文件读写;
	bool readPcdFile(const string &fileName, pcl::PointCloud<pcl::PointXYZI> &pointCloud);
	void mergePcdFilesColoredByFile(const std::string &folderName);
	void mergePcdFilesIntensity(const std::string &folderName);


protected:

private:



};

#endif
