#ifndef REGISTRATION_H
#define REGISTRATION_H

#include "utility.h"
#include "StereoBinaryFeature.h"
#include "searchIndex.h"
#include "VLAD.h"
#include "dataIo.h"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/named_function_params.hpp>
#include <boost/graph/visitors.hpp>
#include <boost/array.hpp>
#include <array>

using namespace  std;
using namespace  Eigen;
using namespace  utility;

namespace pcRegistration
{
	typedef  pair<SBF, SBF> FeaturePair;
	typedef  std::vector<FeaturePair>  vectorFp;
	typedef  std::vector<vectorFp>  doubleVectorFp;

	typedef  boost::property<boost::edge_weight_t, int> EdgeWeightProperty;

	typedef  boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS,boost::no_property, EdgeWeightProperty> RegGraph;
	typedef  RegGraph::edge_descriptor Edge;

	struct CoarseRgOptions
	{
		float tolerantEuclideanDis;//几何一致性的参数;
		float tolerantHammingDis;//允许0.06*size,特征一致性的参数;
		float tolerantOverlap;
		int visualWordsNum;
		int TopSimilarPCNum;
		float tolerantPtDis;//点云合并时的最近两点距离
		int maxMergeNum;
	};

	struct RefineRgOptions
	{
		double euclideanFitnessEpsilon;//前后两次迭代最近点的都是距离变化值，小于该值认为已经收敛;
		double transformationEpsilon;//前后两次迭代旋转矩阵的差异阈值，小于该值认为已经收敛;
		int    maximumIterations;//设置迭代的最大次数
		float  maxCorrespondenceDistance;//对应点的最大距离约束;
		float  stepLength;//maxCorrespondenceDistance 按照该步长逐渐减小
		int    stepError;
	};

	struct RegPair
	{
		PointCloudPair pcPair;
		unsigned int featurePairNum;
		Matrix4f targetToSrc;
		int minSimilarityRank;
		int maxSimilarityRank;
		float similarity;

		RegPair()
		{
			featurePairNum = 0;
			targetToSrc = Matrix4f::Identity(4, 4);
		}
	};

	struct PcPairSimilarity
	{
		int featurePairNum;
		Matrix4f targetToSrc;
		pair<int, int> pcPair;
		PcPairSimilarity()
		{
			featurePairNum = 0;
		}
	};

	struct PointCloudGroup
	{
		int pcGroupIndex;
		std::vector<int> pcIndexes;
		std::vector<pcXYZI> regPCs;
		TribleVectorSBF regBSCs;

		PointCloudGroup()
		{
			pcGroupIndex = 0;
		}
	};
	typedef  PointCloudGroup  PCG;
	typedef  std::vector<RegPair>  vectorRegPair;

	typedef  std::vector<PointCloudGroup>  vectorPCGroup;

	class CReg : public CSearch, public StereoBinaryFeature, public CVLAD, public DataIo
	{
	public:
		CReg(CoarseRgOptions coRgopt, RefineRgOptions reRgopt)
		{
			m_coRgopt = coRgopt;
			m_reRgopt = reRgopt;
		}

		/*基于层次化合并的多视角点云数据配准;*/
		void  multiviewMatchingHPCM(const string &regFileName, const string &transformationName, const std::vector<string> &fileNames, const vectorPCXYZI &pcs, const vectorPCXYZI &pcsForReg,const TribleVectorSBF &bscs, const vectorVLAD &vlads, int baseStationIndex);
		
		
	protected:
		/*根据每个点云的vlad的相似性,找出潜在的相邻点云对;*/
		bool  getCandidatePCRegPairs(const vectorVLAD &vlads, vectorRegPair &pcRegPairs);
		bool  getTopRankedPCRegPairs(const MatrixXf &similarMatrix, const MatrixXi &similarityRank, vectorRegPair &pcRegPairs);

		/*------所有可能的（VLAD相似性约束）两两配准-----*/
		void  exhaustivePairWiseReg(const TribleVectorSBF &bscs, vectorRegPair &pcRegPairs);
	   
		/*-----------------------两两粗配准-------------------------*/
		void pairwiseCoarseReg(const TribleVectorSBF &bscs, RegPair &pcRegPair);

		/*计算两站之间符合特征一致性的Feature点对集合;*/
		bool getFeaturePairsOneWay(const vectorSBF &featSrc, const doubleVectorSBF &featTar, vectorFp &featPairs);
		bool getOptFeaturePairOneWay(const SBF &featSrc, const std::vector<SearchIndex> &searchIndexTar, FeaturePair &featPair);

		/*计算两站之间符合特征一致性的Feature点对集合;*/
		bool getFeaturePairsTwoWay(const vectorSBF &featSrc, const doubleVectorSBF &featTar, vectorFp &featPairs);
		bool getOptFeaturePairTwoWay(const SBF &feat, const std::vector<SearchIndex> &searchIndex, const FeaturePair &featPair);

		/*计算满足特征一致性的点集中,满足几何约束一致性的最大点对集合;*/
		bool  getMaxGeometricConsistencyPairs(const vectorFp &featPairs, vectorFp &gcPairs);
		int   calculateIterationNum(float probability, float tolerance, int minPtNum);//计算RANSAC迭代的次数;
		bool  isCoordinateConsistency(const FeaturePair &pair1, const FeaturePair &pair2);
		void  calculateCoordinateInCS(const Vector3f &pt, const SBF::CoordinateSystem &cs, Vector3f &ptInCS);
		/*根据registration pair 计算转换矩阵;*/
		void  calculateTransformation(const vectorFp &pairs, Eigen::Matrix4f &targetToSrc);
		
		/*--------------PCGroups层次化合并------------------------------*/
		void initializePCGroups(const vectorPCXYZI &pcs, const TribleVectorSBF &bscs, vectorPCGroup &pcGroups);
		void  mergeMostSimilarPcGroups(const vectorRegPair &pcRegPairs, vectorPCGroup &pcGroups, vector<PcPairSimilarity>  &pcPairs);

		//get PcGroupPair With Max Correspondences;
		bool getPcGroupPair(const vectorRegPair &pcRegPairs, const vectorPCGroup &pcGroups, pair<int, int> &pcGroupPair);
		int  calculatePCGSimilarity(const vectorRegPair &pcRegPairs, const PCG &pcGroup1, const PCG &pcGroup2);
		bool getFeaturePair(const vectorRegPair &pcRegPairs, int pcIndex1, int pcIndex2, PcPairSimilarity& pcPair);
		void getTop3SimilarPcPairs(const vectorRegPair &pcRegPairs, const PCG &pcGroupSrc, const PCG &pcGroupTar, vector<PcPairSimilarity> &pcPairs);

		void  mergeSrcPointClouds(const PCG &pcGroupSrc, const vector<PcPairSimilarity>  &pcPairs, pcXYZI & mergedSrcPC);
		void  mergeTarPointClouds(const PCG &pcGroupTar, const vector<PcPairSimilarity>  &pcPairs, pcXYZI & mergedTarPC);

		//利用二进制特征实现两两粗配准;
		int  pairwiseCoarseReg(const PCG &pcgSrc, const PCG &pcgTar, const vector<PcPairSimilarity>  &pcPairs, Matrix4f &targetToSrc);
		//利用ICP实现两两精配准;
		float pairwiseFineReg(const pcXYZIPtr &pcSrc, const pcXYZIPtr &pcTar, const Matrix4f &coarseTrans, Eigen::Matrix4f &fineTrans);

		void  getMergedPcGroup(const PCG &pcGroupSrc, const PCG &pcGroupTar, const Matrix4f &fineTrans,PCG &mergedPcGroup);
		void  getMergedPcPairs(int srcPcIndex, int tarPcIndex, const Matrix4f &fineTrans,int featureNum, vector<PcPairSimilarity>  &pcPairs);
		/*根据转换矩阵计算转换后的特征;*/
		void transformPcGroup(const PCG &pcGroupTar, const Matrix4f &fineTrans, PCG &pcGroupTarTrans);
		void transformBsc(const doubleVectorSBF &bsc, const Matrix4f &fineTrans, doubleVectorSBF &bscTrans);
		void mergePcGroups(const PCG &pcGroupSrc, const PCG &pcGroupTarTrans, PCG &mergedPcGroup);
		void updataPcGroups(const pair<int, int> &pcGroupPair, const PCG &mergedPcGroup, vectorPCGroup &pcGroups);

		/*--------*/
		void regPairsToGraph(const vector<PcPairSimilarity>  &pcPairs, RegGraph &graph);
		void graphToMinSpanningTree(const RegGraph &graph, RegGraph &mst);
		void findPath(int startNodes, int endNodes, const RegGraph &mst, vector<pair<int, int>> &path);

		void getTransformationsToReference(const vector<PcPairSimilarity>  &pcPairs, int baseStationIndex,
			                               const RegGraph &mst, vector<Eigen::Matrix4f> &transformations);

		void getTransformation(const vector<PcPairSimilarity>  &pcPairs, const vector<pair<int, int>> &path, Eigen::Matrix4f &trans);
	
	private:
		RefineRgOptions m_reRgopt;
		CoarseRgOptions m_coRgopt;
		void outputExhausiveRegPC(const vector<string> &fileNames, const vectorPCXYZI &pcs, const vectorRegPair &pcRegPairs);
		void outputRegisteredPointCloudsPcd(const string& fileName, const pcXYZI &pcSrc, const pcXYZI &regPcTar);

		void outputRegprocess(const pair<int, int> &pcGroupPair, const vectorPCGroup &pcGroups, const vector<PcPairSimilarity> &similarPcPairs);
		void outputMultiviewRegisteredPointCloudsPcd(const string &fileName,const std::vector<string> &fileNames, const vector<Eigen::Matrix4f> &transformations, const vectorPCXYZI &pcs);		
		void outputRegisteredPointCloudsPcd(const string& fileName, pcXYZI &regPcTar);
		void outputMatrix(const std::vector<string> &fileNames, const string &fileName, const vector<Eigen::Matrix4f> &transformations);
	};
}
#endif
