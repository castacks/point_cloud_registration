#ifndef SEARCHINDEX_H
#define SEARCHINDEX_H

#include "StereoBinaryFeature.h"

#include <vector>

#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/flann/flann.hpp>

class CSearch
{
public:
	struct SearchIndex
	{
		std::vector<StereoBinaryFeature> featuresInEachBin;
	};

	void buildBscSearchIndex(const doubleVectorSBF &bscs, const int visualWordsNum, std::vector<SearchIndex> &searchIndex);
	void buildBscSearchIndex(const vectorSBF &bscs, const int visualWordsNum, std::vector<SearchIndex> &searchIndex);

protected:
private:
};


#endif