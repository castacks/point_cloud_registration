#ifndef VLAD_H 
#define VLAD_H

#include "utility.h"
#include "featureEncode.h"

#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/flann/flann.hpp>

#include <omp.h>

typedef  pair<int, int> PointCloudPair;
typedef  vector<PointCloudPair>  vectorPCp;
typedef  vector<vectorPCp>  doubleVectorPCp;
typedef  vector<Eigen::VectorXf>  vectorVLAD;


class CVLAD:public CfeatureEncode
{
public:
	
	void calculateVLAD(const TribleVectorSBF &features, int clusterNum, vectorVLAD &vlads);

	void calculateVLAD_DR(const TribleVectorSBF &features, int clusterNum, int compressedDimension, vectorVLAD &vlads);

	void getBscVisualWordIndex(TribleVectorSBF &bsc, const int visualWordsNum);

	void calculateVLADSimilarity(const vectorVLAD &vlads, Eigen::MatrixXf &similarMatrix);
	void getTopRankedPCPairs(const Eigen::MatrixXf &similarMatrix, int topNum, vectorPCp &pcPairs);

protected:

private:
	void  getTrainingData(const TribleVectorSBF &bsc, vectorSBF &trainingData);
	void  BscToFloatBsc(const StereoBinaryFeature &bsc, cv::Mat &fBsc);
	bool  isUniquePCPair(const vectorPCp &pcPairs, const PointCloudPair &candidatePair);
};
#endif
