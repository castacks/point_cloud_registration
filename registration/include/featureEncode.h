#ifndef FEATUREENCODE_H
#define FEATUREENCODE_H

#include "utility.h"
#include "BinaryFeatureExtraction.h"
#include "StereoBinaryFeature.h"

#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/flann/flann.hpp>

class CfeatureEncode
{
public:

	struct  IdSimilarity
	{
		float similarity;
		int pcIndex;
	};
	
	void pcaDReduction(const cv::Mat &dataMat, const int compressedDimension, cv::Mat &CompressedDataMat);

	void getTrainingAndTestingData(const TribleVectorSBF &features,vectorSBF &trainingData,doubleVectorSBF &testingData);

	void kmeansCluster(const cv::Mat &trainingData, int clusterNum, cv::Mat &centers);

	void getRankedSimilarity(const Eigen::MatrixXf &matrixXf, Eigen::MatrixXi &similarityRank);

	void displayClusterResult(const pcXYZI &pointCloud, const cv::Mat &labels, const cv::Mat &centers);

	void outputClusterCenters(const std::string & fileName, const cv::Mat &centers);


	void calculateFeatureSimilarity(const std::vector<Eigen::VectorXf> &vlads, Eigen::MatrixXf &matrixXf);

	void outPutRankedSimilarity(const std::string &fileName, const Eigen::MatrixXf &matrixXf);
	void outPutRankedSimilarity(const std::string &fileName, const Eigen::MatrixXi &similarityRank);
	void outPutSimilarity(const std::string &fileName, const Eigen::MatrixXf &matrixXf);


protected:

	void BscToFloatBsc(const StereoBinaryFeature &bsc, std::vector<float> &fBsc);
	void charToBit(const char num, std::vector<float> &bits);

	void featuresToMat(const vectorSBF &features, cv::Mat &featureMat);


	void localFeatureToVisualWord(const cv::Mat &featureMat, cv::flann::Index &kdtree, std::vector<int> &labels);

	void calculateFeatureResiduals(const cv::Mat &featureMat, const cv::Mat &centers, const std::vector<int> &labels, Eigen::VectorXf &residuals);

	void calculateResidual(const std::vector<float> &localFeature, int visualWordLabel, const std::vector<float> &visualWord, Eigen::VectorXf &residuals);

	float calculateInnerProduct(const Eigen::VectorXf & f1, const Eigen::VectorXf & f2);

	//normalization;
	void powerNormalization(Eigen::VectorXf &vlad);

	void EachClusterL2NormNormalization(const int dimension, Eigen::VectorXf &rvd);	//L2norm 归一化使得每个visualword的贡献相同;

	float calculateVectorNorm1(const std::vector<float> &residual);

	float calculateIncludeAngle(const Eigen::VectorXf & f1, const Eigen::VectorXf & f2);

	void outputCompactFeatures(const std::string & fileName, const std::vector<Eigen::VectorXf> &features);

	//输出特征字符串形式;
	void writeFeatures(const string &fileName, const StereoBinaryFeature &feature);

private:
	

};
#endif